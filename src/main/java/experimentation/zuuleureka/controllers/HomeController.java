package experimentation.zuuleureka.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home")
public class HomeController {

    @PostMapping
    public String greeting(){
        return  "Hello, success authentication" ;
    }

    @GetMapping
    public String greeting1(){
        return  "Hello, you have all access" ;
    }
}
