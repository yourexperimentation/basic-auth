package experimentation.zuuleureka.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class MyUser {
    @Id
    @GeneratedValue
    private long id;

    @NotEmpty
    @NotNull
    private String username;
    @NotEmpty
    @NotNull
    private String password;
    @NotEmpty
    @NotNull
    private String codeMessage;

    @NotEmpty
    @NotNull
    private String role = "USER";
}
